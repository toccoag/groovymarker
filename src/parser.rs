use std::{collections::HashMap, error::Error, usize};

use crate::ast::{BlockStatement, Expression, Statement};

/// Transpiles the given string representing a freemarker template and returns the resulting groovy
/// script. This is a convenience function that handles calling [`TemplateParser::parse`],
/// [`BlockStatement::process`] and [`BlockStatement::append_groovy_string`] for you.
///
/// If the provided template did not generate any statements (e.g. because the template is empty or
/// only contains comments), this simply returns an unallocated empty string.
///
/// # Errors
/// Returns an error if calling [`TemplateParser::parse`] produced an error.
pub fn transpile(
    template: String,
    bindings: &Option<HashMap<String, String>>,
    optimise_paths: bool,
) -> Result<String, Box<dyn Error>> {
    let parser = TemplateParser::new(template);
    match parser.parse()? {
        Some(mut block) => {
            block.process(bindings, optimise_paths);

            let mut buffer = String::new();
            let mut indent = 0;
            block.append_groovy_string(&mut buffer, &mut indent);

            Ok(buffer)
        }
        None => Ok(String::new()),
    }
}

/// Determines whether a character is considered a valid part of an identifier (e.g. a variable)
#[inline]
pub fn char_is_valid_ident(c: char) -> bool {
    c == '_' || c.is_alphanumeric()
}

#[inline]
pub fn char_is_operator(c: char) -> bool {
    c == '=' || c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '&' || c == '!'
}

/// Parser that builds the syntax tree for a freemarker template.
///
/// This is implemented as a state machine that walks each character in the template once, calling
/// [`ParserState::handle`] for each char, returning the [`ParserState`] to use for the next char.
///
/// The [`TemplateParser::parse`] function returns a [`BlockStatement`] representing the root scope
/// or the error encountered while parsing. The BlockStatement should then generally be processed
/// using [`BlockStatement::process`], which performs necessary operations such as handling reassignments,
/// making sure reassignments are marked so that they wont receive the `def` keyword and additional
/// operations such as optimising paths*. The string representation of the equivalent groovy code can
/// then be created and appended to a buffer using [`BlockStatement::append_groovy_string`].
///
/// A TemplateParser instance should not be reused to parse several templates without clearing its
/// state in between.
///
/// *In addition to simply translating the template the transpiler also optimises suboptimal entity paths,
/// e.g.
/// ```freemarker
/// [#assign user = singleBase.relRegistration.relUser]
/// [#assign gender = singleBase.relRegistration.relUser.relGender.unique_id]
/// [#assign lang = singleBase.relRegistration.relUser.relCorrespondence_language.unique_id]
/// [#assign event = singleBase.relRegistration.relEvent.abbreviation]
/// ```
/// becomes
/// ```groovy
/// def relRegistrationGenerated0 = singleBase.relRegistration
/// def user = relRegistrationGenerated0.relUser
/// def gender = user.relGender.unique_id
/// def lang = user.relCorrespondence_language.unique_id
/// def event = relRegistrationGenerated0.relEvent.abbreviation
/// ```
struct TemplateParser {
    template: Vec<char>,
    curr_pos: usize,
    curr_state: Option<ParserState>,
    scope_stack: Vec<BlockStatement>,
    open_quotation: Option<char>,
    nested_bracket_stack: Vec<char>,
}

impl TemplateParser {
    fn new(template: String) -> Self {
        TemplateParser {
            template: template.chars().collect(),
            curr_pos: 0,
            curr_state: Some(ParserState::Scanning),
            scope_stack: Vec::new(),
            open_quotation: None,
            nested_bracket_stack: Vec::new(),
        }
    }

    /// Builds the syntax tree for the template by walking the characters and calling [`ParserState::handle`]
    /// for each char, returning the [`ParserState`] to use for the next char. May return `None` if the provided
    /// template is empty. Else returns a [`BlockStatement`] representing the root scope or the error encountered
    /// while parsing. The BlockStatement should then generally be processed using [`BlockStatement::process`],
    /// which performs necessary operations such as handling reassignments, making sure reassignments are marked
    /// so that they wont receive the `def` keyword and additional operations such as optimising paths*.
    /// The string representation of the equivalent groovy code can then be created and appended to a buffer
    /// using [`BlockStatement::append_groovy_string`].
    ///
    /// # Errors
    /// Returns any error encountered while parsing as soon as it occurs.
    fn parse(mut self) -> Result<Option<BlockStatement>, Box<dyn Error>> {
        if self.template.is_empty() {
            return Ok(None);
        }

        loop {
            let mut curr_char = self.current();
            let curr_state = self.curr_state.take().unwrap();

            // handle built-ins
            if curr_char == '?' && self.open_quotation.is_none() {
                let prev_pos = self.curr_pos;
                match self.handle_built_in() {
                    Ok(replacement) => {
                        let new_pos = self.curr_pos;
                        let range = prev_pos..std::cmp::max(prev_pos + 1, new_pos);

                        if let Some(replacement) = replacement {
                            self.template.splice(range, replacement.chars());
                        } else {
                            self.template.drain(range);
                        }

                        self.curr_pos = prev_pos;
                    }
                    Err(msg) => return Err(msg.into()),
                };

                curr_char = self.current();
            }

            let (is_open_quotation, is_nested_bracket) =
                self.handle_escape(curr_char, &curr_state)?;

            self.curr_state = Some(self.handle_char(
                curr_char,
                curr_state,
                is_open_quotation,
                is_nested_bracket,
            )?);

            if self.curr_pos < self.template.len() - 1 {
                self.curr_pos += 1;
            } else {
                break;
            }
        }

        match self.curr_state {
            Some(ParserState::Scanning) => {}
            Some(state) => {
                return Err(format!("Premature end of template. Current state: {}.", state).into());
            }
            None => unreachable!(),
        };

        if self.scope_stack.len() > 1 {
            Err(
                "Scope stack contains more than one element indicating unclosed block statement"
                    .into(),
            )
        } else {
            Ok(self.scope_stack.pop())
        }
    }

    fn handle_char(
        &mut self,
        curr_char: char,
        curr_state: ParserState,
        is_open_quotation: bool,
        is_nested_bracket: bool,
    ) -> Result<ParserState, Box<dyn Error>> {
        if is_open_quotation || is_nested_bracket {
            match curr_state.handle_escaped(curr_char) {
                Ok(next_state) => Ok(next_state),
                Err(mut msg) => {
                    msg.push_str(&format!(" Failed at pos {}: {}.", self.curr_pos, curr_char));
                    Err(msg.into())
                }
            }
        } else {
            match curr_state.handle(curr_char, self) {
                Ok(next_state) => Ok(next_state),
                Err(mut msg) => {
                    msg.push_str(&format!(" Failed at pos {}: {}.", self.curr_pos, curr_char));
                    Err(msg.into())
                }
            }
        }
    }

    /// Handles characters that switch to literal mode, such as quotes and nested brackets.
    ///
    /// Text within quotes or nested brackets are treated as literals, meaning they are simply
    /// copied from the source template.
    fn handle_escape(
        &mut self,
        curr_char: char,
        curr_state: &ParserState,
    ) -> Result<(bool, bool), Box<dyn Error>> {
        let mut is_open_quotation = self.open_quotation.is_some();
        let mut is_nested_bracket = !self.nested_bracket_stack.is_empty();

        // handle quotations and nested brackets (e.g. [#assign arr = [1, 2, 3]])
        // the quotes themselves are not treated as literals but the brackets are
        if curr_char == '"' || curr_char == '\'' {
            match self.prev() {
                Some(prev_char) if prev_char == '\\' => {}
                _ => match self.open_quotation {
                    Some(quote_char) => {
                        if quote_char == curr_char {
                            self.open_quotation = None;
                            is_open_quotation = false;
                        }
                    }
                    None => self.open_quotation = Some(curr_char),
                },
            }
        } else if !is_open_quotation
            && (curr_char == '[' || curr_char == '(' || curr_char == '{')
            && curr_state.treat_bracket_as_literal()
        {
            self.nested_bracket_stack.push(curr_char);
            if !is_nested_bracket {
                is_nested_bracket = true;
            }
        } else if !is_open_quotation
            && (curr_char == ']' || curr_char == ')' || curr_char == '}')
            && is_nested_bracket
            && curr_state.treat_bracket_as_literal()
        {
            Self::handle_closing_bracket(curr_char, &mut self.nested_bracket_stack)
                .map_err(|msg| Box::<dyn Error>::from(msg))?;
        }

        Ok((is_open_quotation, is_nested_bracket))
    }

    /// Handle freemarker built-ins, such as:
    ///
    /// `?c` -> "" (the ?c built-in is simpy ignored)
    /// `?size` -> `.size()`
    /// `?first` -> `[0]`
    fn handle_built_in(&mut self) -> Result<Option<&'static str>, String> {
        match self.take_until_inclusive(|c| !char_is_valid_ident(c)) {
            Some(word) if word == "c" => {
                // skip ?c
                Ok(None)
            }
            Some(word) if word == "size" => Ok(Some(".size()")),
            Some(word) if word == "first" => Ok(Some("[0]")),
            Some(word) if word == "split" => Ok(Some(".split")),
            Some(word) if word == "number" => Ok(Some(".toDouble()")),
            Some(word) if word == "contains" => Ok(Some(".contains")),
            Some(word) if word == "length" => Ok(Some(".length()")),
            Some(word) if word == "has_content" => {
                // [#if missing?has_content] is equivalent to if (missing) in groovy
                Ok(None)
            }
            Some(word) => Err(format!("Unhandled built-in: '{}'.", word)),
            None => {
                if self.peek_next() == Some('?') {
                    Err(
                        "'??' built-in not supported, referenced properties must exist in groovy."
                            .to_owned(),
                    )
                } else {
                    Err("Dangling built-in operator '?'.".to_string())
                }
            }
        }
    }

    /// Push a new [`BlockStatement`] onto the stack, e.g. when encountering an if-scope.
    fn push_scope(&mut self, block: BlockStatement) {
        // always create root scope
        if self.scope_stack.is_empty() {
            self.scope_stack.push(BlockStatement::Scope(Vec::new()));
        }
        self.scope_stack.push(block);
    }

    /// Append a statement to the current scope. Generally used when finishing parsing a statement.
    fn append_statement(&mut self, statement: Statement) {
        if self.scope_stack.is_empty() {
            let block = BlockStatement::Scope(vec![statement]);
            self.scope_stack.push(block);
        } else {
            self.scope_stack
                .last_mut()
                .unwrap()
                .append_statement(statement);
        }
    }

    /// Return the char the cursor is currently pointing at.
    #[inline]
    fn current(&self) -> char {
        self.template[self.curr_pos]
    }

    /// Return the previous char ahead of the cursor, without moving the cursor.
    #[inline]
    fn prev(&self) -> Option<char> {
        if self.curr_pos > 0 {
            Some(self.template[self.curr_pos - 1])
        } else {
            None
        }
    }

    /// Return the next char after the cursor, without moving the cursor.
    #[inline]
    fn peek_next(&self) -> Option<char> {
        if self.curr_pos < self.template.len() - 1 {
            Some(self.template[self.curr_pos + 1])
        } else {
            None
        }
    }

    /// Return the next char after the cursor, moving the cursor to that char.
    #[inline]
    fn take_next(&mut self) -> Option<char> {
        if self.curr_pos < self.template.len() - 1 {
            self.curr_pos += 1;
            Some(self.template[self.curr_pos])
        } else {
            None
        }
    }

    /// Takes the next word (starting at the next char, not the current) and advances
    /// to the char after the word. Might return `None` if at the end of the template
    /// or followed by a character that is not a valid identifier, such as whitespace.
    /// If `None` is returned, the position is not advanced.
    fn take_next_word(&mut self) -> Option<String> {
        self.take_until_exclusive(|c| !char_is_valid_ident(c))
    }

    /// Takes all characters until (excluding) the first character that matches the given
    /// condition and advances the cursor to that character.
    ///
    /// "inclusive" refers to including the delimiting char when advancing the cursor, thus
    /// advancing to the first char that is not included in the returned string, used when
    /// the first char that didn't match should count as handled.
    fn take_until_inclusive<F: Fn(char) -> bool>(&mut self, exit_condition: F) -> Option<String> {
        self.take_until(true, exit_condition)
    }

    /// Takes all characters until (excluding) the first character that matches the given
    /// condition and advances the cursor to the last character included in the returned string.
    ///
    /// "exclusive" refers to excluding the delimiting char when advancing the cursor and only
    /// advancing to the last match, used when the first char that didn't match should not count
    /// as handled.
    fn take_until_exclusive<F: Fn(char) -> bool>(&mut self, exit_condition: F) -> Option<String> {
        self.take_until(false, exit_condition)
    }

    fn take_until<F: Fn(char) -> bool>(
        &mut self,
        inclusive: bool,
        exit_condition: F,
    ) -> Option<String> {
        let start_index = self.curr_pos + 1;
        let end = self.template.len();
        let mut end_index = None;

        for i in start_index..end {
            let curr_char = self.template[i];

            if exit_condition(curr_char) {
                end_index = Some(i);
                break;
            }
        }

        let end_index = end_index.unwrap_or(end);
        if start_index < end_index {
            if inclusive {
                self.curr_pos = end_index;
            } else {
                self.curr_pos = end_index - 1;
            }

            Some(self.template[start_index..end_index].iter().collect())
        } else {
            None
        }
    }

    fn handle_closing_bracket(
        curr_char: char,
        nested_bracket_stack: &mut Vec<char>,
    ) -> Result<(), String> {
        match nested_bracket_stack.pop() {
            Some(opened_bracket) => {
                if opened_bracket == '(' {
                    if curr_char != ')' {
                        return Err(format!(
                            "Closing bracket mismatch, expected ')' but found '{}'.",
                            curr_char
                        ));
                    }
                } else if opened_bracket == '[' {
                    if curr_char != ']' {
                        return Err(format!(
                            "Closing bracket mismatch, expected ']' but found '{}'.",
                            curr_char
                        ));
                    }
                } else if opened_bracket == '{' && curr_char != '}' {
                    return Err(format!(
                        "Closing bracket mismatch, expected '}}' but found '{}'.",
                        curr_char
                    ));
                }
            }
            None => {
                return Err(format!(
                    "Closing bracket mismatch, unexpected closing bracket '{}'.",
                    curr_char
                ));
            }
        };

        Ok(())
    }
}

/// The current state of the [`TemplateParser`] that decides how a char is handled.
#[derive(strum_macros::Display)]
enum ParserState {
    Scanning,
    ClosingTag,
    Assignment {
        assignment: Assignment,
        is_rhs: bool,
    },
    IfStatement {
        else_if: bool,
        condition: String,
    },
    ElseStatement,
    ListStatement {
        list_expression: String,
        capture_var: String,
        completed_list_expression: bool,
        completed_capture_var: bool,
        completed_as_keyword: bool,
    },
    // e.g. ${expression}
    Interpolation {
        expression: String,
        nested_bracket_stack: Vec<char>,
    },
}

impl ParserState {
    /// Handle the current char according to the current state and return the state used to handle the next char.
    fn handle(mut self, curr_char: char, parser: &mut TemplateParser) -> Result<Self, String> {
        match self {
            ParserState::Scanning => self.handle_scanning_state(curr_char, parser),
            ParserState::ClosingTag => self.handle_closing_tag(curr_char, parser),
            ParserState::Assignment {
                ref mut assignment,
                ref mut is_rhs,
            } => Self::handle_assignment(curr_char, assignment, is_rhs, parser)
                .map(|state| state.unwrap_or(self)),
            ParserState::IfStatement {
                ref else_if,
                ref mut condition,
            } => Ok(
                Self::handle_if_statement(curr_char, else_if, condition, parser).unwrap_or(self),
            ),
            ParserState::ElseStatement => self.handle_else_statement(curr_char, parser),
            ParserState::ListStatement {
                ref mut list_expression,
                ref mut capture_var,
                ref mut completed_list_expression,
                ref mut completed_capture_var,
                ref mut completed_as_keyword,
            } => Self::handle_list_statement(
                curr_char,
                list_expression,
                capture_var,
                completed_list_expression,
                completed_capture_var,
                completed_as_keyword,
                parser,
            )
            .map(|state| state.unwrap_or(self)),
            ParserState::Interpolation {
                ref mut expression,
                ref mut nested_bracket_stack,
            } => Self::handle_interpolation(curr_char, expression, nested_bracket_stack, parser)
                .map(|state| state.unwrap_or(self)),
        }
    }

    /// Handle the current char according to the current state. The parser uses this function while inside an open quotation
    /// or nested brackets, implementations generally just append the char to the current expression.
    fn handle_escaped(mut self, curr_char: char) -> Result<Self, String> {
        match self {
            ParserState::Scanning => Err(format!("Unexpected literal '{}'.", curr_char)),
            ParserState::ClosingTag => Err(format!("Unexpected literal '{}'.", curr_char)),
            ParserState::Assignment {
                ref mut assignment,
                ref mut is_rhs,
                ..
            } => {
                if *is_rhs {
                    assignment.append_rhs(curr_char);
                } else {
                    assignment.append_lhs(curr_char);
                }
                Ok(self)
            }
            ParserState::IfStatement {
                ref mut condition, ..
            } => {
                condition.push(curr_char);
                Ok(self)
            }
            ParserState::ElseStatement => Err(format!("Unexpected literal '{}'.", curr_char)),
            ParserState::ListStatement {
                ref mut list_expression,
                ref mut capture_var,
                ref mut completed_list_expression,
                ref mut completed_capture_var,
                ..
            } => {
                if *completed_capture_var {
                    Err(format!("Unexpected literal '{}'.", curr_char))
                } else if *completed_list_expression {
                    capture_var.push(curr_char);
                    Ok(self)
                } else {
                    list_expression.push(curr_char);
                    Ok(self)
                }
            }
            ParserState::Interpolation {
                ref mut expression, ..
            } => {
                expression.push(curr_char);
                Ok(self)
            }
        }
    }

    /// Whether the state treats square brackets as literal expressions, e.g.
    /// For input like `[#if arr[1] == "foo"] or [#elseif map["foo"]=="bar"] or [#list two_dimensions[0] as foo]`
    /// the states return nested brackets as literal input.
    fn treat_bracket_as_literal(&self) -> bool {
        match self {
            ParserState::Scanning
            | ParserState::ClosingTag
            | ParserState::ElseStatement
            | ParserState::Interpolation { .. } => false,
            ParserState::Assignment { .. }
            | ParserState::IfStatement { .. }
            | ParserState::ListStatement { .. } => true,
        }
    }

    fn handle_scanning_state(
        self,
        curr_char: char,
        parser: &mut TemplateParser,
    ) -> Result<Self, String> {
        if curr_char.is_whitespace() {
            return Ok(self);
        }

        if curr_char == '[' {
            match parser.take_next() {
                Some(c) if c == '#' => match parser.take_next_word() {
                    Some(word) => {
                        if word == "assign" {
                            Ok(ParserState::Assignment {
                                assignment: Assignment::new(),
                                is_rhs: false,
                            })
                        } else if word == "if" {
                            Ok(ParserState::IfStatement {
                                else_if: false,
                                condition: String::new(),
                            })
                        } else if word == "elseif" || word == "else" {
                            match parser.scope_stack.pop() {
                                Some(block) => match block {
                                    BlockStatement::IfStatement { .. } => {
                                        parser.append_statement(Statement::BlockStatement(
                                            block,
                                        ));

                                        if word == "elseif" {
                                            Ok(ParserState::IfStatement {
                                                else_if: true,
                                                condition: String::new(),
                                            })
                                        } else {
                                            Ok(ParserState::ElseStatement)
                                        }
                                    }
                                    _ => Err(
                                        "Illegal else or elseif directive outside of if directive or in incorrect order."
                                            .to_owned(),
                                    ),
                                },
                                _ => {
                                    Err("Illegal else or elseif directive outside of if directive or in incorrect order."
                                        .to_owned())
                                }
                            }
                        } else if word == "list" {
                            Ok(ParserState::ListStatement {
                                list_expression: String::new(),
                                capture_var: String::new(),
                                completed_list_expression: false,
                                completed_capture_var: false,
                                completed_as_keyword: false,
                            })
                        } else {
                            Err(format!("Unhandled directive '{}'.", word))
                        }
                    }
                    None => {
                        if parser.peek_next() == Some('-') {
                            parser.take_next();
                            match parser.take_next() {
                                Some(c) if c == '-' => {
                                    // encountered comment: [#--, skip until first --]
                                    // (the delimiting --] cannot be escaped, quotes do not have to be checked)
                                    loop {
                                        parser.take_until_inclusive(|c| c == '-');

                                        if parser.take_next() == Some('-') && parser.take_next() == Some(']') {
                                            break Ok(self);
                                        } else if parser.peek_next().is_none() {
                                            break Err("Unclosed comment.".to_owned());
                                        }
                                    }
                                }
                                Some(c) => {
                                    Err(format!("Unexpected input '{}' after '[#-', expected '-' marking the start of a comment.", c))
                                }
                                None => {
                                    Err("Dangling '[#-' in input.".to_owned())
                                }
                            }
                        } else {
                            Err("Dangling '[#' in input.".to_owned())
                        }
                    }
                },
                Some(c) if c == '/' => Ok(ParserState::ClosingTag),
                Some(c) if c == '@' => {
                    Err("Invalid input '[@', user defined directives not supported.".to_owned())
                }
                Some(c) => Err(format!(
                    "Invalid input '{}' after '[', expected: '#' or '/'.",
                    c
                )),
                None => Err("Dangling '[' in input.".to_owned()),
            }
        } else if curr_char == '$' {
            match parser.take_next() {
                Some(next_char) if next_char == '{' => Ok(ParserState::Interpolation {
                    expression: String::new(),
                    nested_bracket_stack: Vec::new(),
                }),
                Some(c) => Err(format!(
                    "Unexpected input after '$', expected '{{' but found {}.",
                    c
                )),
                None => Err("Premature end of template, expected '{' after '$'.".to_owned()),
            }
        } else {
            Err(format!(
                "Unexpected input '{}', expected '[' or '$'.",
                curr_char
            ))
        }
    }

    fn handle_closing_tag(
        self,
        curr_char: char,
        parser: &mut TemplateParser,
    ) -> Result<Self, String> {
        if curr_char == '#' {
            match parser.take_next_word() {
                Some(word) => {
                    if word == "if" {
                        Self::pop_scope_for_closed_tag(parser, |parser, block| {
                            match block {
                                BlockStatement::IfStatement { .. } | BlockStatement::ElseStatement(..) => {
                                    parser.append_statement(Statement::BlockStatement(block));
                                    Ok(ParserState::Scanning)
                                }
                                _ => Err("Closing tag mismatch, encountered closing if tag outside of if statement.".to_owned())
                            }
                        })
                    } else if word == "list" {
                        Self::pop_scope_for_closed_tag(parser, |parser, block| {
                            match block {
                                BlockStatement::ListStatement { .. } => {
                                    parser.append_statement(Statement::BlockStatement(block));
                                    Ok(ParserState::Scanning)
                                }
                                _ => Err("Closing tag mismatch, encountered closing list tag outside of list statement.".to_owned())
                            }
                        })
                    } else {
                        Err(format!("Unexpected closing tag '{}'.", word))
                    }
                }
                None => Err("Dangling '[/#' in input.".to_owned()),
            }
        } else {
            Err(format!(
                "Invalid input '{}' after '[/', expected: '#'.",
                curr_char
            ))
        }
    }

    fn pop_scope_for_closed_tag<
        F: FnOnce(&mut TemplateParser, BlockStatement) -> Result<Self, String>,
    >(
        parser: &mut TemplateParser,
        scope_stack_handler: F,
    ) -> Result<Self, String> {
        if parser.peek_next() == Some(']') {
            parser.take_next();
            match parser.scope_stack.pop() {
                Some(block) => scope_stack_handler(parser, block),
                None => Err(
                    "Closing tag mismatch, encountered closing tag does not match current scope."
                        .to_owned(),
                ),
            }
        } else {
            match parser.take_until_inclusive(|c| c == ']') {
                Some(trailing) => {
                    if trailing.trim().is_empty() {
                        match parser.scope_stack.pop() {
                            Some(block) => scope_stack_handler(parser, block),
                            None => Err("Closing tag mismatch, encountered closing tag does not match current scope.".to_owned())
                        }
                    } else {
                        Err(format!("Unexpected content in closing tag: {}.", trailing))
                    }
                }
                None => Err("Closing tag not closed by ']'.".to_owned()),
            }
        }
    }

    fn handle_assignment(
        mut curr_char: char,
        assignment: &mut Assignment,
        is_rhs: &mut bool,
        parser: &mut TemplateParser,
    ) -> Result<Option<Self>, String> {
        if curr_char.is_whitespace() {
            let last_non_whitespace = parser.prev();
            // skip chained whitespace
            if parser.peek_next().map_or(false, |c| c.is_whitespace()) {
                parser.take_until_exclusive(|c| !c.is_whitespace());
                curr_char = parser.current();
            }

            // ignore leading whitespace
            //
            // ignore whitespace ahead of operator, e.g. this space can be ignored
            // [#assign var = foo]
            //             ^
            // but this space separates assignments
            // [#assign var1 var2]
            //              ^
            if !assignment.is_empty(*is_rhs)
                && parser
                    .peek_next()
                    .map_or(false, |next| char_is_valid_ident(next))
            {
                if *is_rhs && last_non_whitespace.map_or(false, |c| char_is_operator(c)) {
                    assignment.append_rhs(curr_char);
                } else {
                    assignment.add_assignment();
                    *is_rhs = false;
                }
            } else if *is_rhs && !assignment.is_empty(*is_rhs) {
                assignment.append_rhs(curr_char);
            }
            Ok(None)
        } else if curr_char == ']' {
            // move the assignment out of the state and leave a new empty assignment, since empty strings do not
            // allocate anything this is pretty cheap and the new assignment is never used as the state is dropped
            let assignment = std::mem::replace(assignment, Assignment::new());
            parser.append_statement(assignment.into_statement());
            // step out of assignment state
            Ok(Some(ParserState::Scanning))
        } else if char_is_operator(curr_char) {
            let next_char = parser.peek_next();
            // ignore `/` when closing assignment statement, e.g. [#assing foo = "bar" /]
            //                                                                         ^
            if curr_char == '/' && next_char == Some(']') {
                Ok(None)
            } else if curr_char != '=' {
                // allow all operations on rhs, e.g. [#assign x = 1 + 2]
                // illegal assignment operations on rhs (e.g. [#assign x = 1 += 2]) will be handled when encountering the '='
                if *is_rhs {
                    assignment.append_rhs(curr_char);
                    Ok(None)
                } else if next_char == Some('=') {
                    *is_rhs = true;
                    assignment.append_op(curr_char);
                    assignment.append_op(parser.take_next().unwrap());
                    Ok(None)
                } else {
                    assignment.append_lhs(curr_char);
                    Ok(None)
                }
            } else if *is_rhs {
                if parser.peek_next() == Some('=') {
                    // == operator
                    let next = parser.take_next().unwrap();
                    assignment.append_rhs(curr_char);
                    assignment.append_rhs(next);
                    Ok(None)
                } else if parser.prev() == Some('!') {
                    // != operator
                    assignment.append_rhs(curr_char);
                    Ok(None)
                } else {
                    Err("Unexpected '=' in rhs of assignment.".to_owned())
                }
            } else if assignment.ref_lhs().is_empty() {
                Err("Unexpected '=' following empty lhs of assigment.".to_owned())
            } else {
                assignment.append_op(curr_char);
                *is_rhs = true;
                Ok(None)
            }
        } else if curr_char == ',' {
            assignment.add_assignment();
            *is_rhs = false;
            Ok(None)
        } else {
            if *is_rhs {
                assignment.append_rhs(curr_char);
            } else {
                assignment.append_lhs(curr_char);
            }
            Ok(None)
        }
    }

    fn handle_if_statement(
        curr_char: char,
        else_if: &bool,
        condition: &mut String,
        parser: &mut TemplateParser,
    ) -> Option<Self> {
        // ignore leading / trailing whitespace and compact other whitespace
        if curr_char.is_whitespace() {
            if condition.is_empty() {
                None
            } else {
                // skip to last whitespace char
                parser.take_until_exclusive(|c| !c.is_whitespace());
                // ignore trailing whitespace
                if parser.peek_next() != Some(']') {
                    condition.push(' ');
                }
                None
            }
        } else if curr_char == ']' {
            parser.push_scope(BlockStatement::IfStatement {
                else_if: *else_if,
                condition: Expression::LiteralExpression(std::mem::replace(
                    condition,
                    String::new(),
                )),
                statements: Vec::new(),
            });
            Some(ParserState::Scanning)
        } else {
            condition.push(curr_char);
            None
        }
    }

    fn handle_else_statement(
        self,
        curr_char: char,
        parser: &mut TemplateParser,
    ) -> Result<Self, String> {
        if curr_char.is_whitespace() {
            Ok(self)
        } else if curr_char == ']' {
            parser.push_scope(BlockStatement::ElseStatement(Vec::new()));
            Ok(ParserState::Scanning)
        } else {
            Err(format!(
                "Unexpected input '{}' in else directive.",
                curr_char
            ))
        }
    }

    fn handle_list_statement(
        curr_char: char,
        list_expression: &mut String,
        capture_var: &mut String,
        completed_list_expression: &mut bool,
        completed_capture_var: &mut bool,
        completed_as_keyword: &mut bool,
        parser: &mut TemplateParser,
    ) -> Result<Option<Self>, String> {
        #[allow(clippy::if_same_then_else)]
        if curr_char.is_whitespace() {
            // skip to last whitespace char
            parser.take_until_exclusive(|c| !c.is_whitespace());
            if parser.peek_next() == Some('[') {
                // ignore whitespace leading [
                // e.g. [#list two_dimensional  [0] as foo]
                //                             ^
                Ok(None)
            } else if *completed_list_expression && *completed_capture_var {
                // ignore trailing whitespace
                Ok(None)
            } else if *completed_list_expression {
                // ignore leading whitespace for capture var expression
                if capture_var.is_empty() {
                    Ok(None)
                } else {
                    // terminate capture var
                    *completed_capture_var = true;
                    Ok(None)
                }
            } else if list_expression.is_empty() {
                Ok(None)
            } else {
                *completed_list_expression = true;
                Ok(None)
            }
        } else if curr_char == ']' {
            if !list_expression.is_empty() && !capture_var.is_empty() {
                parser.push_scope(BlockStatement::ListStatement {
                    list_expression: Expression::LiteralExpression(std::mem::replace(
                        list_expression,
                        String::new(),
                    )),
                    capture_var: Expression::LiteralExpression(std::mem::replace(
                        capture_var,
                        String::new(),
                    )),
                    statements: Vec::new(),
                });
                Ok(Some(ParserState::Scanning))
            } else {
                Err("Unexpected end of list expression".to_owned())
            }
        } else if *completed_list_expression && *completed_capture_var {
            Err(format!(
                "Unexpected input {} in list expression, expected ']'.",
                curr_char
            ))
        } else if *completed_list_expression {
            if *completed_as_keyword {
                capture_var.push(curr_char);
                Ok(None)
            } else {
                // "as" keyword is case sensitive
                if curr_char == 'a'
                    && parser.take_next() == Some('s')
                    && parser.take_next().map_or(false, |c| c.is_whitespace())
                {
                    *completed_as_keyword = true;
                    Ok(None)
                } else {
                    Err(format!(
                        "Unexpected input {} in list expression, expected 'as'.",
                        parser.current()
                    ))
                }
            }
        } else {
            list_expression.push(curr_char);
            Ok(None)
        }
    }

    fn handle_interpolation(
        curr_char: char,
        expression: &mut String,
        nested_bracket_stack: &mut Vec<char>,
        parser: &mut TemplateParser,
    ) -> Result<Option<Self>, String> {
        if curr_char == '[' || curr_char == '(' || curr_char == '{' {
            nested_bracket_stack.push(curr_char);
            expression.push(curr_char);
            Ok(None)
        } else if curr_char == ']' || curr_char == ')' || curr_char == '}' {
            if curr_char == '}' && nested_bracket_stack.is_empty() {
                parser.append_statement(Statement::ExpressionStatement(
                    Expression::LiteralExpression(std::mem::replace(expression, String::new())),
                ));
                Ok(Some(ParserState::Scanning))
            } else {
                TemplateParser::handle_closing_bracket(curr_char, nested_bracket_stack)?;
                expression.push(curr_char);
                Ok(None)
            }
        } else if nested_bracket_stack.is_empty() {
            if curr_char.is_whitespace() {
                if expression.is_empty() {
                    Ok(None)
                } else {
                    parser.take_until_exclusive(|c| !c.is_whitespace());
                    if !(parser.peek_next() == Some('}') && nested_bracket_stack.is_empty()) {
                        expression.push(curr_char);
                    }
                    Ok(None)
                }
            } else {
                expression.push(curr_char);
                Ok(None)
            }
        } else {
            expression.push(curr_char);
            Ok(None)
        }
    }
}

/// Helper type used when parsing freemarker assign directive calls.
enum Assignment {
    Single {
        lhs: String,
        rhs: String,
        op: String,
    },
    List(Vec<(String, String, String)>),
}

impl Assignment {
    fn new() -> Self {
        Self::Single {
            lhs: String::new(),
            rhs: String::new(),
            op: String::new(),
        }
    }

    fn into_statement(self) -> Statement {
        match self {
            Assignment::Single { lhs, rhs, op } => {
                match Self::assigment_to_expression(lhs, rhs, op) {
                    Some(expr) => Statement::ExpressionStatement(expr),
                    // return empty expression list, since empty vectors do not allocate until pushing an element, the overhead is minimal
                    None => Statement::ExpressionList(Vec::new()),
                }
            }
            Assignment::List(assignments) => {
                let expressions = assignments
                    .into_iter()
                    .map(|(lhs, rhs, op)| Self::assigment_to_expression(lhs, rhs, op))
                    .filter(|expr| expr.is_some())
                    .map(|expr| expr.unwrap())
                    .collect();
                Statement::ExpressionList(expressions)
            }
        }
    }

    fn assigment_to_expression(lhs: String, rhs: String, op: String) -> Option<Expression> {
        let lhs_set = !lhs.is_empty();
        let rhs_set = !rhs.is_empty();
        let simple_assignment = op == "=";

        if lhs_set && rhs_set {
            Some(Expression::BinaryExpression {
                lhs,
                rhs,
                op,
                is_declaration: simple_assignment,
            })
        } else if lhs_set {
            Some(Expression::UnaryExpression {
                expression: lhs,
                is_declaration: true,
            })
        } else {
            // empty assignments (e.g. `[#assign]`) return None and are ignored
            None
        }
    }

    fn append_lhs(&mut self, c: char) {
        match self {
            Assignment::Single { lhs, .. } => lhs.push(c),
            Assignment::List(assignments) => {
                let curr_assignment = assignments.last_mut().unwrap();
                curr_assignment.0.push(c);
            }
        }
    }

    fn append_rhs(&mut self, c: char) {
        match self {
            Assignment::Single { rhs, .. } => rhs.push(c),
            Assignment::List(assignments) => {
                let curr_assignment = assignments.last_mut().unwrap();
                curr_assignment.1.push(c);
            }
        }
    }

    fn append_op(&mut self, c: char) {
        match self {
            Assignment::Single { op, .. } => op.push(c),
            Assignment::List(assignments) => {
                let curr_assignment = assignments.last_mut().unwrap();
                curr_assignment.2.push(c);
            }
        }
    }

    fn ref_lhs(&self) -> &str {
        match self {
            Assignment::Single { lhs, .. } => &lhs,
            Assignment::List(assignments) => {
                let curr_assignment = assignments.last().unwrap();
                &curr_assignment.0
            }
        }
    }

    // Keep for completion's sake.
    #[allow(dead_code)]
    fn ref_rhs(&self) -> &str {
        match self {
            Assignment::Single { rhs, .. } => &rhs,
            Assignment::List(assignments) => {
                let curr_assignment = assignments.last().unwrap();
                &curr_assignment.1
            }
        }
    }

    fn add_assignment(&mut self) {
        match self {
            Assignment::Single { lhs, rhs, op } => {
                // move the existing single assignment into a vector and replace this single assigment with an assignment list
                // moves the strings into the vector by replacing the strings of this assignment with an empty string,
                // this is cheaper than simply cloning the strings because it requires no allocations
                let mut assignment_vec = vec![(
                    std::mem::replace(lhs, String::new()),
                    std::mem::replace(rhs, String::new()),
                    std::mem::replace(op, String::new()),
                )];
                // add the new assignment
                assignment_vec.push((String::new(), String::new(), String::new()));
                *self = Assignment::List(assignment_vec)
            }
            Assignment::List(ref mut assignment_vec) => {
                assignment_vec.push((String::new(), String::new(), String::new()))
            }
        }
    }

    fn is_empty(&self, is_rhs: bool) -> bool {
        match self {
            Assignment::Single { lhs, rhs, .. } => {
                if is_rhs {
                    rhs.is_empty()
                } else {
                    lhs.is_empty()
                }
            }
            Assignment::List(assignments) => {
                if is_rhs {
                    assignments.last().unwrap().1.is_empty()
                } else {
                    assignments.last().unwrap().0.is_empty()
                }
            }
        }
    }
}
