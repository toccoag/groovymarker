#![allow(clippy::redundant_closure)]

use std::{collections::HashMap, fs::File, io::Write, usize};

use rand::Rng;
use structopt::StructOpt;

use parser::transpile;

mod ast;
mod parser;
mod path;

/// Freemarker to groovy transpiler that may translate any freemarker template that solely consists of
/// directive calls (currently: [#assign], [#if], [#else if], [#else], [#list]) and interpolations
/// (e.g. `${expression}`) and does not contain any raw text.
///
/// The `transpile-template` subcommand can be used to transpile a given template and print the result.
///
/// The `migrate-table` subcommand can be used to transpile all freemarker templates in the given column
/// of the given table and generated a changeset for each successfully migrated row and create a changelog
/// file in the current directory.
///
/// To get help with a specific subcommand type `groovymarker help migrate-table`.
#[derive(StructOpt, Debug)]
#[structopt(name = "groovymarker")]
enum Opt {
    /// Transpile all freemarker templates in the given column of the given table and generated a changeset
    /// for each successfully migrated row and create a changelog file in the current directory.
    MigrateTable {
        /// Author to use for changeset creation.
        #[structopt(short = "a", long)]
        author: String,
        /// Nice version to use for changeset creation.
        #[structopt(short = "v", long)]
        version: String,
        /// Hostname for the database server, defaults to localhost.
        #[structopt(short = "h", long, default_value = "localhost")]
        host: String,
        /// Database user name.
        #[structopt(short = "u", long)]
        user: String,
        /// Password for the database user.
        #[structopt(short = "p", long)]
        password: String,
        #[structopt(short = "d", long)]
        /// Name of the database.
        db_name: String,
        #[structopt(short = "t", long)]
        /// Name of the table to migrate.
        table: String,
        #[structopt(short = "f", long)]
        /// Name of the column containing the freemarker templates.
        field: String,
        /// Bindings for path rewrites. For example, for wage scale conditions it makes sense
        /// to bind `singleBase.relLecturer_booking` to `booking`.
        /// Usage: `-b singleBase.relLecturer_booking:booking,singleBase.relEvent:event`.
        #[structopt(short = "b", long, parse(try_from_str = parse_binding_map))]
        bindings: Option<HashMap<String, String>>,
        /// Whether to skip optimising entity paths, leaving them as is without analysing common sub paths.
        #[structopt(short = "n", long)]
        no_optimise: bool,
    },
    /// Transpile a given template and print the result.
    TranspileTemplate {
        /// Bindings for path rewrites. For example, for wage scale conditions it makes sense
        /// to bind `singleBase.relLecturer_booking` to `booking`.
        /// Usage: `-b singleBase.relLecturer_booking:booking,singleBase.relEvent:event`.
        #[structopt(short = "b", long, parse(try_from_str = parse_binding_map))]
        bindings: Option<HashMap<String, String>>,
        /// Freemarker template to transpile, supplied as command input.
        template: String,
        /// Whether to skip optimising entity paths, leaving them as is without analysing common sub paths.
        #[structopt(short = "n", long)]
        no_optimise: bool,
    },
}

#[cfg(windows)]
const LINE_SEPARATOR: &str = "\r\n";
#[cfg(not(windows))]
const LINE_SEPARATOR: &str = "\n";

const CHANGELOG_HEADER: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog xmlns="http://www.tocco.ch/xml/ns/dbchangelog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.tocco.ch/xml/ns/dbchangelog http://www.tocco.ch/xml/ns/dbchangelog/dbchangelog-tocco-3.1.xsd">"#;
const CHANGELOG_FOOTER: &str = "</databaseChangeLog>";

macro_rules! format_change_set {
    () => {
r#"
  <changeSet author="{author}" dbms="postgresql" id="{table}_{unique_id}_groovy_migration-groovymarkergenid{id}/{version}">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">select count(*) from {table} where unique_id = '{unique_id}'</sqlCheck>
    </preConditions>
    <update tableName="{table}">
      <column name="{field}" value="{groovy}"/>
      <where>unique_id = '{unique_id}'</where>
    </update>
  </changeSet>
"#
    };
}

const INDENT: usize = 4;

fn main() {
    let instant_full = std::time::Instant::now();
    match Opt::from_args() {
        Opt::MigrateTable {
            author,
            version,
            host,
            user,
            password,
            db_name,
            table,
            field,
            bindings,
            no_optimise,
        } => {
            let mut client = match postgres::Client::connect(
                &format!(
                    "host='{host}' user='{user}' password='{password}' dbname='{db_name}'",
                    host = host,
                    user = user,
                    password = password,
                    db_name = db_name
                ),
                postgres::NoTls,
            ) {
                Ok(client) => client,
                Err(e) => {
                    eprintln!("Error connecting to postgres: {}", e);
                    return;
                }
            };

            let mut successful_migrations = Vec::new();
            let mut failed_migrations = Vec::new();

            let query = format!(
                "SELECT unique_id, {field} FROM {table}",
                field = field,
                table = table
            );
            match client.query(query.as_str(), &[]) {
                Ok(rows) => {
                    let instant_transpile_phase = std::time::Instant::now();
                    for row in rows {
                        let unique_id: &str = row.get(0);
                        let template: &str = row.get(1);

                        match transpile(String::from(template), &bindings, !no_optimise) {
                            Ok(groovy) => {
                                let escaped_groovy = xml::escape::escape_str_attribute(&groovy);
                                use rand::distributions::Alphanumeric;
                                let id: String = std::iter::repeat(())
                                    .map(|_| rand::thread_rng().sample(Alphanumeric))
                                    .map(char::from)
                                    .take(7)
                                    .collect();

                                let change_set = format!(
                                    format_change_set!(),
                                    author = author,
                                    table = table,
                                    unique_id = unique_id,
                                    id = id,
                                    version = version,
                                    field = field,
                                    groovy = escaped_groovy
                                );
                                successful_migrations.push(change_set);
                            }
                            Err(e) => {
                                eprintln!("Failed migration for '{}': {}", unique_id, e);
                                failed_migrations.push(String::from(unique_id));
                            }
                        }
                    }

                    println!(
                        "transpiled all templates after {}µs",
                        instant_transpile_phase.elapsed().as_micros()
                    );
                }
                Err(e) => {
                    eprintln!("Error executing query: {}", e);
                    return;
                }
            }

            let successful_migrations_len = successful_migrations.len();
            if !successful_migrations.is_empty() {
                let mut changelog_buffer = String::from(CHANGELOG_HEADER);
                let change_sets = successful_migrations.concat();
                changelog_buffer.push_str(&change_sets);
                changelog_buffer.push_str(CHANGELOG_FOOTER);

                match File::create(
                    format! {"{version}_migrate_{table}_groovymarker_generated.xml", version = version, table = table},
                ) {
                    Ok(mut file) => {
                        if let Err(e) = file.write_all(changelog_buffer.as_bytes()) {
                            eprintln!("Failed to write changelog to file: {}", e);
                            return;
                        }
                    }
                    Err(e) => {
                        eprintln!("Failed to create changelog file: {}", e);
                        return;
                    }
                }
            }

            println!("-----------------------------------------------------------------");
            println!(
                "Successfully generated changesets for {} elements",
                successful_migrations_len
            );
            eprintln!("Migration failed for {} elements", failed_migrations.len());

            if !failed_migrations.is_empty() {
                eprintln!("Migration failed for the following elements:");

                for unique_id in failed_migrations {
                    eprintln!("{}", unique_id);
                }
            }

            println!(
                "Migration done after {}ms",
                instant_full.elapsed().as_millis()
            );
        }
        Opt::TranspileTemplate {
            template,
            bindings,
            no_optimise,
        } => {
            match transpile(template, &bindings, !no_optimise) {
                Ok(groovy) => println!("{}", groovy),
                Err(e) => eprintln!("{}", e),
            };
        }
    }
}

fn parse_binding_map(
    string_representation: &str,
) -> std::result::Result<HashMap<String, String>, String> {
    let mut map = HashMap::new();

    for binding_str in string_representation.split(',') {
        let binding_str = binding_str.trim();

        if !binding_str.is_empty() {
            let mapping: Vec<&str> = binding_str.split(':').collect();

            if mapping.len() == 2 {
                map.insert(String::from(mapping[0]), String::from(mapping[1]));
            } else {
                return Err("Bindings argument is not formatted correctly, needs to be in the form of 'src1:target1,src2:target2'".to_owned());
            }
        }
    }

    Ok(map)
}
