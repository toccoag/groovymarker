use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::Ordering;

use crate::{
    ast::{Expression, Statement, StatementCell},
    parser::char_is_valid_ident,
};

lazy_static! {
    pub static ref IS_PATH_REGEX: Regex =
        Regex::new(r#"^(([a-zA-Z]\w*)(\(.*\))?\.([a-zA-Z]\w*)(\(.*\))?)+$"#).unwrap();
}

/// Structures all given paths as [`PathTreeNode`] instances. Creates path trees where each node
/// represents a common sub path of all of its children. The following rule applies to the returned
/// nodes and all of their children: all super paths held by this node are completely different after
/// the path represented by this node. The returned Vec represents the direct children of a conceptual
/// root node representing the empty path, as such each returned PathTreeNode represents the root of
/// a path tree of existing or newly created paths. Check [`PathNode::adopt_super_path`] for more
/// documentation on how the structure of the trees are created.
pub fn create_path_graph(paths: Vec<Path>) -> Vec<PathTreeNode> {
    let mut root_node = RootPathNode::default();
    for path in paths {
        let current_node = PathTreeNode::Existing {
            path,
            super_paths: Vec::new(),
            equivalent_paths: Vec::new(),
        };

        root_node.adopt_super_path(current_node);
    }

    root_node.get_super_paths_owned()
}

/// Applies the collected [`PathApplyOp`] operations to the statement vector by mapping the statements
/// to [`StatementCell`] instances, applying all insert / remove operations to them (such as to not
/// change the length of the Vec, thus invalidating the indices referenced by the operations), then
/// collecting the StatementCell Vec back to a Statement Vec.
pub fn apply_path_ops(path_apply_ops: Vec<PathApplyOp>, statements: &mut Vec<Statement>) {
    if !path_apply_ops.is_empty() {
        // steal current statement vector
        let stolen_statements = std::mem::replace(statements, Vec::new());
        // map statement vector to vector of StatementCells, allowing remove and insert operations that do not
        // invalidate the indices held by the operations because they do not change the length of the vector
        let mut statement_cells: Vec<StatementCell> = stolen_statements
            .into_iter()
            .map(|statement| StatementCell::Single(Some(statement)))
            .collect();

        let mut expected_statement_count = statement_cells.len();

        for path_apply_op in path_apply_ops {
            match path_apply_op {
                PathApplyOp::CreateStatement {
                    statement,
                    target_index,
                } => {
                    expected_statement_count += 1;
                    statement_cells[target_index].insert_statement(statement)
                }
                PathApplyOp::ReorderStatement {
                    original_index,
                    target_index,
                } => {
                    let removed_statement = statement_cells[original_index]
                        .remove()
                        .expect("Could not remove statement for reordering since the statement at the target index does not exist.");
                    statement_cells[target_index].insert_statement(removed_statement);
                }
            }
        }

        let mut output_statements = Vec::with_capacity(expected_statement_count);
        for statement_cell in statement_cells {
            match statement_cell {
                StatementCell::Single(Some(statement)) => output_statements.push(statement),
                StatementCell::Single(None) => {}
                StatementCell::Multiple {
                    original,
                    mut inserted,
                } => {
                    output_statements.append(&mut inserted);
                    if let Some(original) = original {
                        output_statements.push(original);
                    }
                }
            }
        }

        *statements = output_statements;
    }
}

/// Creates operations that create new assignments for new paths or reorder existing statements where necessary.
/// This generates the expressions for assignments to new paths and substitutes the sub path represented by the
/// generated alias in all super paths.
///
/// In essence, this performs all operations that do not require mutating the statements vector and returns all
/// operations that do. Since paths hold references to the rhs of binary expressions (i.e. the path_strings),
/// borrows of paths must end before mutating the statement vector holding the statements that contain the binary
/// expressions.
pub fn apply_path_aliases_and_generate_statement_ops(
    sub_paths: Vec<PathTreeNode>,
) -> Vec<PathApplyOp> {
    let mut path_apply_ops: Vec<PathApplyOp> = Vec::new();
    let mut generated_idents_counter = 0;
    for sub_path in sub_paths {
        apply_path_tree_node(
            sub_path,
            &mut path_apply_ops,
            None,
            &mut generated_idents_counter,
        );
    }

    path_apply_ops
}

/// Recursively handles all [`PathTreeNode`] by generating an alias for new paths and applying the alias of sub paths
/// to the rhs of super paths, e.g. for `relEventGenerated0 = singleBase.relLecturer_booking.relEvent;
/// eventType = singleBase.relLecturer_booking.relEvent.relEvent_type.unique_id`, eventType is transformed to
/// `eventType = relEventGenerated0.relEvent_type.unique_id`.
///
/// Collects all [`PathApplyOp`] that need to be applied later, such as inserting new statements or reordering statements.
fn apply_path_tree_node(
    sub_path: PathTreeNode,
    path_apply_ops: &mut Vec<PathApplyOp>,
    rhs: Option<String>,
    counter: &mut usize,
) -> usize {
    let (path, super_paths, equivalent_paths) = match sub_path {
        PathTreeNode::Existing {
            path,
            super_paths,
            equivalent_paths,
        } => (path, super_paths, Some(equivalent_paths)),
        PathTreeNode::New { path, super_paths } => (path, super_paths, None),
    };

    let (lhs, statement_index) = match path {
        Path::Existing {
            lhs,
            path_string,
            statement_index,
            ..
        } => {
            let original_rhs_len = path_string.len();
            if let Some(rhs) = rhs {
                *path_string = rhs;
            }

            let original_statement_index = statement_index;
            let mut new_statement_index = statement_index;

            let lhs = lhs.clone();
            for super_node in super_paths {
                let mut super_path_rhs = lhs.clone();
                let super_path = super_node.get_path();
                super_path_rhs.push_str(&super_path.get_path_string()[original_rhs_len..]);
                let super_path_index =
                    apply_path_tree_node(super_node, path_apply_ops, Some(super_path_rhs), counter);

                if super_path_index <= new_statement_index {
                    if super_path_index >= 1 {
                        new_statement_index = super_path_index - 1;
                    } else {
                        new_statement_index = 0;
                    }
                }
            }

            if original_statement_index != new_statement_index {
                path_apply_ops.push(PathApplyOp::ReorderStatement {
                    original_index: original_statement_index,
                    target_index: new_statement_index,
                });
            }

            (lhs, new_statement_index)
        }
        Path::New {
            mut path_string, ..
        } => {
            let original_rhs_len = path_string.len();
            if let Some(rhs) = rhs {
                path_string = rhs;
            }

            let mut ident = String::from(
                path_string
                    .split('.')
                    .last()
                    .unwrap()
                    .trim_matches(|c| !char_is_valid_ident(c)),
            );
            ident.push_str("Generated");
            ident.push_str(&counter.to_string());
            *counter += 1;

            let expression = Expression::BinaryExpression {
                lhs: ident.clone(),
                rhs: path_string,
                op: String::from("="),
                is_declaration: true,
            };

            let mut lowest_statement_index: Option<usize> = None;

            for super_path in super_paths {
                let mut super_path_rhs = ident.clone();
                super_path_rhs
                    .push_str(&super_path.get_path().get_path_string()[original_rhs_len..]);
                let statement_index =
                    apply_path_tree_node(super_path, path_apply_ops, Some(super_path_rhs), counter);

                match lowest_statement_index {
                    Some(lowest_statement_index) if lowest_statement_index <= statement_index => {}
                    Some(ref mut lowest_statement_index) => {
                        *lowest_statement_index = statement_index
                    }
                    None => {
                        lowest_statement_index = Some(statement_index);
                    }
                };
            }

            // new paths are only created for common sub paths, it does not make sense for them to not have super paths
            let lowest_statement_index = lowest_statement_index.expect("Could not determine statement index for new path as it unexpectedly does not have super paths");

            path_apply_ops.push(PathApplyOp::CreateStatement {
                statement: Statement::ExpressionStatement(expression),
                target_index: lowest_statement_index,
            });

            (ident, lowest_statement_index)
        }
        // only the root path is empty
        Path::Empty => unreachable!(),
    };

    // assign all equivalent paths to this path
    if let Some(equivalent_paths) = equivalent_paths {
        for mut equivalent_path in equivalent_paths {
            match equivalent_path.get_path_mut() {
                Path::Existing { path_string, .. } => {
                    **path_string = lhs.clone();
                }
                _ => unreachable!("Encountered equivalent path that isn't an existing path"),
            }
        }
    }

    statement_index
}

/// Trait to be implemented for any type that may within the PathTree (currently [`RootPathNode`] and
/// [`PathTreeNode`]) exposing common operations.
pub trait PathNode<'a> {
    /// Adopt a [`PathTreeNode`] that represents a super path of the path represented by this PathNode
    /// (for [`RootPathNode`] this is any node). This function checks the super paths already added to
    /// this node and handles the following 3 cases:
    /// * The `new_super_node` is a super path of a previously added path, in this case the `new_super_node`
    ///   is adopted as a super path of the previously added path recursively.
    /// * The `new_super_node` is a sub path of a previously added path, in this case the previously
    ///   added path is removed as super path of this node and adopted as super path by the `new_super_node`,
    ///   then the `new_super_node` is adopted as super path of this path.
    /// * The `new_super_node` has a common sub path with a previously added path that is longer than
    ///   this path (and longer than 1 fragment, relevant for the root path), in this case the previously
    ///   added path is removed from this node and a new node is created for the common sub path, which
    ///   adopts both the `new_super_node` and the removed node as super paths and is then added as
    ///   super path of this path, e.g. consider the following nodes:
    ///   <pre>
    ///   singleBase.relReservation_lecturer_booking
    ///    |_ singleBase.relReservation_lecturer_booking.relLecturer_booking.relEvent
    ///   and then the following path is adopted: 'singleBase.relReservation_lecturer_booking.relLecturer_booking.relUser'
    ///   this leads to the following structure:
    ///   singleBase.relReservation_lecturer_booking
    ///    |_ singleBase.relReservation_lecturer_booking.relLecturer_booking
    ///       |- singleBase.relReservation_lecturer_booking.relLecturer_booking.relEvent
    ///       |_ singleBase.relReservation_lecturer_booking.relLecturer_booking.relUser
    ///   </pre>
    ///
    /// Else, the `new_super_node` is simply added to the collection of this nodes super paths.
    fn adopt_super_path(&mut self, mut new_super_node: PathTreeNode<'a>) {
        let self_path_len = self.get_path().len();
        let super_paths = self.get_super_paths_mut();
        let new_super_path = new_super_node.get_path();

        let mut found_lower_super_path = None;
        let mut found_common_sub_path = None;
        for (i, super_node) in super_paths.iter_mut().enumerate() {
            let super_path = super_node.get_path();

            let is_super_path = new_super_path.is_super_path(super_path);

            if is_super_path {
                super_node.adopt_super_path(new_super_node);
                return;
            }

            let is_sub_path = super_path.is_super_path(new_super_path);

            if is_sub_path {
                found_lower_super_path = Some(i);
                break;
            }

            let common_sub_path_len = super_path.common_sub_path(new_super_path);

            if super_path.len() == new_super_path.len() && common_sub_path_len == super_path.len() {
                match super_node {
                    PathTreeNode::Existing {
                        equivalent_paths, ..
                    } => equivalent_paths.push(new_super_node),
                    // encountered an equivalent existing path for a path for which a new path was created,
                    // replace new path with existing path
                    PathTreeNode::New { super_paths, .. } => {
                        // steal super paths of new path
                        std::mem::swap(super_paths, new_super_node.get_super_paths_mut());
                        *super_node = new_super_node;
                    }
                }
                return;
            }

            let common_sub_path_exceeds_current_path = common_sub_path_len > self_path_len;
            if common_sub_path_exceeds_current_path && common_sub_path_len > 1 {
                let common = super_node.get_path().create_sub_path(common_sub_path_len);

                found_common_sub_path = Some((i, common));
                break;
            }
        }

        if let Some(common_sub_path) = found_common_sub_path {
            let other_path = super_paths.remove(common_sub_path.0);
            let path = common_sub_path.1;

            let mut sub_path = PathTreeNode::New {
                path,
                super_paths: Vec::with_capacity(2),
            };

            sub_path.adopt_super_path(other_path);
            sub_path.adopt_super_path(new_super_node);

            super_paths.push(sub_path);
        } else if let Some(lower_super_index) = found_lower_super_path {
            let lower_super = super_paths.remove(lower_super_index);
            new_super_node.adopt_super_path(lower_super);
            super_paths.push(new_super_node);
        } else {
            super_paths.push(new_super_node);
        }
    }

    fn get_path(&self) -> &Path;

    fn get_super_paths(&self) -> &Vec<PathTreeNode<'a>>;

    fn get_super_paths_mut(&mut self) -> &mut Vec<PathTreeNode<'a>>;

    fn get_super_paths_owned(self) -> Vec<PathTreeNode<'a>>;
}

/// Conceptual root node for the path tree representing an empty path that has each other path as
/// super path, the direct children of this node are those [`PathTreeNode`] instances returned by
/// [`create_path_graph`].
#[derive(Default)]
pub struct RootPathNode<'a> {
    super_paths: Vec<PathTreeNode<'a>>,
}

impl<'a> PathNode<'a> for RootPathNode<'a> {
    fn get_path(&self) -> &Path {
        static EMPTY_PATH: Path = Path::Empty;
        &EMPTY_PATH
    }

    fn get_super_paths(&self) -> &Vec<PathTreeNode<'a>> {
        &self.super_paths
    }

    fn get_super_paths_mut(&mut self) -> &mut Vec<PathTreeNode<'a>> {
        &mut self.super_paths
    }

    fn get_super_paths_owned(self) -> Vec<PathTreeNode<'a>> {
        self.super_paths
    }
}

/// Represents an entity path within a hierarchical path tree. Holds all paths that are
/// are a super path of this path. All super paths (and all of their super paths) have
/// this path as a common sub-path.
///
/// When adding a path that is the super path of a current super path, it is added as that
/// path's super path recursively instead; when adding a path that has a common sub path
/// with an existing super path that is longer than this path, the common sub path of those
/// two paths is added as new super path with those two paths as super path; when adding
/// a path that is the sub path of a current super path, that means the path is closer to
/// this path than the current super path, thus the path steals the current super path as its
/// own super paths and is added as a new super path of this path.
/// As such, all super paths held by this node are completely different after the path represented
/// by this node.
///
/// Check [`PathNode::adopt_super_path`] for more documentation on how the structure of the trees are created.
pub enum PathTreeNode<'a> {
    Existing {
        path: Path<'a>,
        super_paths: Vec<PathTreeNode<'a>>,
        equivalent_paths: Vec<PathTreeNode<'a>>,
    },
    New {
        path: Path<'a>,
        super_paths: Vec<PathTreeNode<'a>>,
    },
}

impl<'a> PathNode<'a> for PathTreeNode<'a> {
    fn get_path(&self) -> &Path {
        match self {
            PathTreeNode::Existing { path, .. } => path,
            PathTreeNode::New { path, .. } => path,
        }
    }

    fn get_super_paths(&self) -> &Vec<PathTreeNode<'a>> {
        match self {
            PathTreeNode::Existing { super_paths, .. } | PathTreeNode::New { super_paths, .. } => {
                super_paths
            }
        }
    }

    fn get_super_paths_mut(&mut self) -> &mut Vec<PathTreeNode<'a>> {
        match self {
            PathTreeNode::Existing { super_paths, .. } | PathTreeNode::New { super_paths, .. } => {
                super_paths
            }
        }
    }

    fn get_super_paths_owned(self) -> Vec<PathTreeNode<'a>> {
        match self {
            PathTreeNode::Existing { super_paths, .. } | PathTreeNode::New { super_paths, .. } => {
                super_paths
            }
        }
    }
}

impl<'a> PathTreeNode<'a> {
    pub fn get_path_mut(&mut self) -> &mut Path<'a> {
        match self {
            PathTreeNode::Existing { path, .. } => path,
            PathTreeNode::New { path, .. } => path,
        }
    }
}

pub enum Path<'a> {
    Existing {
        lhs: &'a String,
        path_string: &'a mut String,
        // indices marking the location of path separators, note that this does not include the last fragment
        path_indices: Vec<usize>,
        statement_index: usize,
    },
    New {
        path_string: String,
        // indices marking the location of path separators, note that this does not include the last fragment
        path_indices: Vec<usize>,
    },
    Empty,
}

impl Path<'_> {
    /// Return the length of the path in path fragments. This is equal to the length of the path_indices vec + 1
    /// to account for the last fragment after the last path separator.
    pub fn len(&self) -> usize {
        match self {
            Path::Existing { path_indices, .. } | Path::New { path_indices, .. } => {
                path_indices.len() + 1
            }
            Path::Empty => 0,
        }
    }

    pub fn iter(&self) -> PathIter<'_> {
        match self {
            Path::Existing {
                path_string,
                path_indices,
                ..
            } => PathIter::new(*path_string, path_indices),
            Path::New {
                path_string,
                path_indices,
                ..
            } => PathIter::new(&*path_string, path_indices),
            Path::Empty => PathIter::empty(),
        }
    }

    pub fn is_super_path(&self, sub: &Path) -> bool {
        let mut fragments = self.iter();
        let mut sub_fragments = sub.iter();

        if self.len() > sub.len() {
            for _ in 0..sub.len() {
                let fragment = fragments.next().unwrap();
                let sub_fragment = sub_fragments.next().unwrap();

                if fragment != sub_fragment
                    || path_fragment_is_function_call(fragment)
                    || path_fragment_is_function_call(sub_fragment)
                {
                    return false;
                }
            }

            true
        } else {
            false
        }
    }

    /// Return the length of the longest common sub path in number of path fragments.
    /// e.g.
    /// ```
    /// let path1 = Path::New {
    ///     path_string: String::new("singleBase.relReservation_lecturer_booking.relLecturer_booking.relEvent.pk"),
    ///     path_indices: vec![10, 42, 62, 71],
    /// };
    /// let path2 = Path::New {
    ///     path_string: String::new("singleBase.relReservation_lecturer_booking.relLecturer_booking.relUser.pk"),
    ///     path_indices: vec![10, 42, 62, 70],
    /// };
    ///
    /// assert_eq!(path1.common_sub_path(&path2), 3);
    ///
    /// let eq_path1 = Path::New {
    ///     path_string: String::new("singleBase.relLecturer_booking.relEvent.pk"),
    ///     path_indices: vec![10, 30, 39],
    /// };
    /// let eq_path2 = Path::New {
    ///     path_string: String::new("singleBase.relLecturer_booking.relEvent.pk"),
    ///     path_indices: vec![10, 30, 39],
    /// };
    ///
    /// assert_eq!(eq_path1.common_sub_path(eq_path2), 4);
    /// ```
    pub fn common_sub_path(&self, other: &Path) -> usize {
        let mut self_fragments = self.iter();
        let mut other_fragments = other.iter();

        let end = std::cmp::min(self.len(), other.len());
        for i in 0..end {
            let self_fragment = self_fragments.next().unwrap();
            let other_fragment = other_fragments.next().unwrap();
            if self_fragment != other_fragment
                || path_fragment_is_function_call(self_fragment)
                || path_fragment_is_function_call(other_fragment)
            {
                return i;
            }
        }

        end
    }

    pub fn get_path_string(&self) -> &str {
        match self {
            Path::Existing { path_string, .. } => path_string,
            Path::New { path_string, .. } => path_string,
            Path::Empty => "",
        }
    }

    pub fn get_path_indices(&self) -> &[usize] {
        match self {
            Path::Existing { path_indices, .. } | Path::New { path_indices, .. } => path_indices,
            Path::Empty => &[],
        }
    }

    pub fn create_sub_path<'s, 'p>(&'s self, len: usize) -> Path<'p> {
        self.try_create_sub_path(len).unwrap_or_else(|| {
            panic!(
                "Could not create sub path due to out of bounds length {} for path with length {}",
                len,
                self.len()
            )
        })
    }

    pub fn try_create_sub_path<'s, 'p>(&'s self, len: usize) -> Option<Path<'p>> {
        if len > self.len() || len == 0 {
            None
        } else {
            let path_string = self.get_path_string();
            let path_indices = self.get_path_indices();
            let is_full_path = len == self.len();

            let end = if is_full_path {
                self.get_path_string().len()
            } else {
                path_indices[len - 1]
            };

            let sub_path_string = String::from(&path_string[0..end]);
            let sub_path_indices = if is_full_path {
                path_indices.to_vec()
            } else if len > 1 {
                let mut dest = vec![0_usize; len - 1];
                dest.copy_from_slice(&path_indices[0..len - 1]);
                dest
            } else {
                Vec::new()
            };

            Some(Path::New {
                path_string: sub_path_string,
                path_indices: sub_path_indices,
            })
        }
    }
}

#[inline]
fn path_fragment_is_function_call(path_fragment: &str) -> bool {
    path_fragment.ends_with(')')
}

/// Iterator that walks the fragments of a path.
///
/// Since the Paths already keep track of the indices of the path separator '.' this
/// iterator can be used to iterate its fragments more efficiently than splitting the string.
///
/// e.g.
/// ```
/// let path_string = String::from("singleBase.relEvent.abbreviation");
/// let path_indices = vec![10, 19];
/// let mut path_iter = PathIter::new(&path_string, &path_indices);
///
/// assert_eq!(path_iter.next(), Some("singleBase"));
/// assert_eq!(path_iter.next(), Some("relEvent"));
/// assert_eq!(path_iter.next(), Some("abbreviation"));
/// assert_eq!(path_iter.next(), None);
/// ```
pub struct PathIter<'a> {
    path_string: &'a str,
    path_indices: &'a [usize],
    next_index: Option<usize>,
    i: usize,
}

impl<'a> PathIter<'a> {
    /// Create a new PathIter for the provided path string with the provided indices for the path separator '.'
    /// used to split the string and yield each fragment of the path excluding the '.'.
    fn new(path_string: &'a str, path_indices: &'a [usize]) -> Self {
        Self {
            path_string,
            path_indices,
            next_index: Some(*path_indices.first().unwrap_or(&path_string.len())),
            i: 0,
        }
    }

    /// Create an empty iterator that does not yield any elements, which is different from calling [`PathIter::new`]
    /// with an empty path_string and path_indices Vec as that iterator would yield an empty string.
    fn empty() -> Self {
        Self {
            path_string: "",
            path_indices: &[],
            next_index: None,
            i: 0,
        }
    }
}

impl<'a> Iterator for PathIter<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<<Self as std::iter::Iterator>::Item> {
        if let Some(curr_index) = self.next_index {
            let start = if self.i > 0 {
                // use the end index of the previous path fragment and skip the '.'
                self.path_indices[self.i - 1] + 1
            } else {
                0
            };

            let curr_fragment = &self.path_string[start..curr_index];

            self.i += 1;

            // the path indices represent the indices of the path separator, thus do not include the last fragment
            self.next_index = match self.i.cmp(&self.path_indices.len()) {
                Ordering::Less => Some(self.path_indices[self.i]),
                Ordering::Equal => Some(self.path_string.len()),
                Ordering::Greater => None,
            };

            Some(curr_fragment)
        } else {
            None
        }
    }
}

/// Represents an action that needs to be applied to paths later since they perform operations
/// that would invalidated any statement indices, such as inserting a new statement or reordering
/// statements. These operations require moving the Statements to a different Vec where they are
/// mapped to a [`StatementCell`], which allows insert and remove operations without invalidating
/// the indices of Vec because the size is never changed. Hence, these actions must be performed
/// after the lifetime of all [`Path`] instances has ended, as they hold references to the rhs
/// of binary expressions represented by the path.
pub enum PathApplyOp {
    CreateStatement {
        statement: Statement,
        target_index: usize,
    },
    ReorderStatement {
        original_index: usize,
        target_index: usize,
    },
}

#[cfg(test)]
mod tests {
    use super::{create_path_graph, Path, PathIter, PathNode, PathTreeNode};

    #[test]
    fn test_path_iter() {
        let path_string = String::from("singleBase.relEvent.abbreviation");
        let path_indices = vec![10, 19];
        let mut path_iter = PathIter::new(&path_string, &path_indices);

        assert_eq!(path_iter.next(), Some("singleBase"));
        assert_eq!(path_iter.next(), Some("relEvent"));
        assert_eq!(path_iter.next(), Some("abbreviation"));
        assert_eq!(path_iter.next(), None);
    }

    #[test]
    fn test_single_fragment_path_iter() {
        let path_string = String::from("singleBase");
        let path_indices = Vec::new();
        let mut path_iter = PathIter::new(&path_string, &path_indices);

        assert_eq!(path_iter.next(), Some("singleBase"));
        assert_eq!(path_iter.next(), None);
    }

    #[test]
    fn test_empty_path_iter() {
        let mut path_iter_empty = PathIter::empty();
        assert_eq!(path_iter_empty.next(), None);

        let path_string = String::new();
        let path_indices = Vec::new();
        let mut path_iter = PathIter::new(&path_string, &path_indices);
        assert_eq!(path_iter.next(), Some(""));
        assert_eq!(path_iter.next(), None);
    }

    #[test]
    fn test_create_path_graph() {
        let path1 = Path::Existing {
            lhs: &mut String::from("user_status"),
            path_string: &mut String::from(
                "singleBase.relLecturer_booking.relUser.relUser_status.unique_id",
            ),
            path_indices: vec![10, 30, 38, 53],
            statement_index: 0,
        };
        let path2 = Path::Existing {
            lhs: &mut String::from("user_status_type"),
            path_string: &mut String::from("singleBase.relLecturer_booking.relUser.relUser_status.relUser_status_type.unique_id"),
            path_indices: vec![10, 30, 38, 53, 73],
            statement_index: 0,
        };
        let path3 = Path::Existing {
            lhs: &mut String::from("user_status_type2"),
            path_string: &mut String::from("singleBase.relLecturer_booking.relUser.relUser_status.relUser_status_type.unique_id"),
            path_indices: vec![10, 30, 38, 53, 73],
            statement_index: 0,
        };
        let path4 = Path::Existing {
            lhs: &mut String::from("gender"),
            path_string: &mut String::from(
                "singleBase.relLecturer_booking.relUser.relGender.unique_id",
            ),
            path_indices: vec![10, 30, 38, 48],
            statement_index: 0,
        };
        let path5 = Path::Existing {
            lhs: &mut String::from("other"),
            path_string: &mut String::from(
                "singleBase.relLecturer_booking.relUser.relUser_status.relOther.unique_id",
            ),
            path_indices: vec![10, 30, 38, 53, 62],
            statement_index: 0,
        };

        let paths = vec![path1, path2, path3, path4, path5];
        let path_graph = create_path_graph(paths);

        assert_eq!(path_graph.len(), 1);

        let root = &path_graph[0];
        assert_eq!(
            root.get_path().get_path_string(),
            "singleBase.relLecturer_booking.relUser"
        );

        assert_eq!(root.get_super_paths().len(), 2);
        for super_path in root.get_super_paths() {
            let curr_path = super_path.get_path();
            let curr_path_str = curr_path.get_path_string();

            assert!(
                curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status"
                    || curr_path_str
                        == "singleBase.relLecturer_booking.relUser.relGender.unique_id"
            );

            let super_super_paths = super_path.get_super_paths();
            if curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status" {
                assert_eq!(super_super_paths.len(), 3);

                let mut found_equivalent_path = false;
                for super_super_path in super_super_paths {
                    let curr_path = super_super_path.get_path();
                    let curr_path_str = curr_path.get_path_string();

                    assert!(
                        curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status.unique_id"
                            || curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status.relUser_status_type.unique_id"
                            || curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status.relOther.unique_id"
                    );
                    assert_eq!(super_super_path.get_super_paths().len(), 0);

                    if curr_path_str == "singleBase.relLecturer_booking.relUser.relUser_status.relUser_status_type.unique_id" {
                        if let PathTreeNode::Existing { ref equivalent_paths, .. } = super_super_path {
                            assert_eq!(equivalent_paths.len(), 1);
                            assert_eq!(
                                equivalent_paths.last().unwrap().get_path().get_path_string(),
                                "singleBase.relLecturer_booking.relUser.relUser_status.relUser_status_type.unique_id"
                            );
                            found_equivalent_path = true;
                        }
                    }
                }
                assert!(found_equivalent_path);
            } else {
                assert_eq!(super_super_paths.len(), 0);
            }
        }
    }
}
