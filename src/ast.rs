use std::collections::{HashMap, HashSet};

use crate::{
    parser::char_is_operator,
    path::{
        apply_path_aliases_and_generate_statement_ops, apply_path_ops, create_path_graph, Path,
        IS_PATH_REGEX,
    },
    INDENT, LINE_SEPARATOR,
};

/// Code block / scope with special support for statements that have a scope such as if-statements.
pub enum BlockStatement {
    Scope(Vec<Statement>),
    IfStatement {
        else_if: bool,
        condition: Expression,
        statements: Vec<Statement>,
    },
    ElseStatement(Vec<Statement>),
    // freemarker foreach loop, e.g. [#list foo as bar]
    ListStatement {
        list_expression: Expression,
        capture_var: Expression,
        statements: Vec<Statement>,
    },
}

impl BlockStatement {
    /// Appends the groovy string representation of this BlockStatement to the provided buffer.
    ///
    /// This calls [`Statement::append_groovy_string`] for all contained statements and increments the
    /// indent level where appropriate.
    pub fn append_groovy_string(&self, buffer: &mut String, indent: &mut usize) {
        match self {
            BlockStatement::Scope(statements) => {
                for statement in statements {
                    statement.append_groovy_string(buffer, indent);
                }
            }
            BlockStatement::IfStatement {
                else_if,
                condition,
                statements,
            } => {
                buffer.push_str(&" ".repeat(*indent * INDENT));
                if *else_if {
                    buffer.push_str("else if ");
                } else {
                    buffer.push_str("if ");
                }

                buffer.push('(');
                condition.append_groovy_string(buffer);
                buffer.push(')');
                buffer.push_str(" {");
                buffer.push_str(LINE_SEPARATOR);

                *indent += 1;
                for statement in statements {
                    statement.append_groovy_string(buffer, indent);
                }
                *indent -= 1;

                buffer.push_str(&" ".repeat(*indent * INDENT));
                buffer.push('}');
                buffer.push_str(LINE_SEPARATOR);
            }
            BlockStatement::ElseStatement(statements) => {
                buffer.push_str(&" ".repeat(*indent * INDENT));
                buffer.push_str("else {");
                buffer.push_str(LINE_SEPARATOR);

                *indent += 1;
                for statement in statements {
                    statement.append_groovy_string(buffer, indent);
                }
                *indent -= 1;

                buffer.push_str(&" ".repeat(*indent * INDENT));
                buffer.push('}');
                buffer.push_str(LINE_SEPARATOR);
            }
            BlockStatement::ListStatement {
                list_expression,
                capture_var,
                statements,
            } => {
                buffer.push_str(&" ".repeat(*indent * INDENT));
                buffer.push_str("for (");
                buffer.push_str("def ");
                capture_var.append_groovy_string(buffer);
                buffer.push_str(" : ");
                list_expression.append_groovy_string(buffer);
                buffer.push_str(") {");
                buffer.push_str(LINE_SEPARATOR);

                *indent += 1;
                for statement in statements {
                    statement.append_groovy_string(buffer, indent);
                }
                *indent -= 1;

                buffer.push_str(&" ".repeat(*indent * INDENT));
                buffer.push('}');
                buffer.push_str(LINE_SEPARATOR);
            }
        }
    }

    pub fn append_statement(&mut self, statement: Statement) {
        match self {
            BlockStatement::Scope(statements)
            | BlockStatement::ElseStatement(statements)
            | BlockStatement::IfStatement { statements, .. }
            | BlockStatement::ListStatement { statements, .. } => statements.push(statement),
        }
    }

    // keep for completion's sake
    #[allow(dead_code)]
    pub fn get_statements(&self) -> &Vec<Statement> {
        match self {
            Self::Scope(statements)
            | Self::IfStatement { statements, .. }
            | Self::ElseStatement(statements)
            | Self::ListStatement { statements, .. } => statements,
        }
    }

    pub fn get_statements_mut(&mut self) -> &mut Vec<Statement> {
        match self {
            Self::Scope(statements)
            | Self::IfStatement { statements, .. }
            | Self::ElseStatement(statements)
            | Self::ListStatement { statements, .. } => statements,
        }
    }

    /// Processes all statements in the block performing necessary operations,
    ///
    /// such as handling re-assignments making sure they are not marked as initial assignments,
    /// e.g. so that `[#assign x = 3][#assign x = 4]` becomes  `def x = 3; x = 4;` instead of
    /// `def x = 3; def x = 4`
    ///
    /// and performing additional operations such as optimising entity paths, e.g. so that
    /// `[#assign eventWageLevel=singleBase.relLecturer_booking.relEvent.relEvent_wage_level/]
    /// [#assign userRegion=singleBase.relLecturer_booking.relEvent.relCost_center.relUser_region/]`
    /// becomes
    /// `def relEvent = singleBase.relLecturer_booking.relEvent
    /// def eventWageLevel = relEvent.relEvent_wage_level
    /// def userRegion = relEvent.relCost_center.relUser_region`
    ///
    /// Additionally, this is where bindings are applied to the rhs of matching assignments, so that,
    /// given the bindings `singleBase.relLecturer_booking:booking,singleBase:reservationBooking`, the template
    /// ```freemarker
    /// [#assign event = singleBase.relLecturer_booking.relEvent]
    /// [#assign reservation = singleBase.relReservation]
    /// ```
    /// is transpiled to the following groovy script:
    /// ```groovy
    /// def event = booking.relEvent
    /// def reservation = reservationBooking.relReservation
    /// ```
    /// Naturally, bindings are applied before optimising paths.
    pub fn process(&mut self, bindings: &Option<HashMap<String, String>>, optimise_paths: bool) {
        process_block(self, &mut Vec::new(), bindings, optimise_paths)
    }
}

/// Function that processes the provided [`BlockStatement`] and recursively processes all contained
/// [`Statement::BlockStatement`], this function is initially called by invoking [`BlockStatement::process`].
fn process_block<'a>(
    block: &'a mut BlockStatement,
    variable_scope_stack: &mut Vec<HashSet<String>>,
    bindings: &Option<HashMap<String, String>>,
    optimise_paths: bool,
) {
    variable_scope_stack.push(HashSet::new());
    let statements = block.get_statements_mut();
    let mut paths = Vec::new();

    for (idx, statement) in statements.iter_mut().enumerate() {
        match statement {
            Statement::ExpressionStatement(expression) => {
                process_expression(expression, variable_scope_stack, &mut paths, bindings, idx)
            }
            Statement::ExpressionList(expressions) => {
                for expression in expressions {
                    process_expression(expression, variable_scope_stack, &mut paths, bindings, idx)
                }
            }
            Statement::BlockStatement(block) => {
                process_block(block, variable_scope_stack, bindings, optimise_paths)
            }
        }
    }

    variable_scope_stack.pop();

    if optimise_paths {
        let path_graph = create_path_graph(paths);
        let path_apply_ops = apply_path_aliases_and_generate_statement_ops(path_graph);
        apply_path_ops(path_apply_ops, statements);
    }
}

/// Processes the given expression.
///
/// For binary expressions representing declarations this checks whether the variable has already been
/// declared within the current scope stack, setting `is_declaration` to false if that is the case.
/// Additionally this checks whether the rhs of an assignment is a path, collecting the [`Path`]
/// for potential optimisation.
///
/// For unary expressions marked as declarations this performs the same check to determine whether the
/// variable has already been declared as for binary expressions.
fn process_expression<'a>(
    expression: &'a mut Expression,
    variable_scope_stack: &mut Vec<HashSet<String>>,
    paths: &mut Vec<Path<'a>>,
    bindings: &Option<HashMap<String, String>>,
    statement_index: usize,
) {
    match expression {
        Expression::BinaryExpression {
            lhs,
            rhs,
            op,
            is_declaration,
        } => {
            filter_declaration(is_declaration, lhs, variable_scope_stack);

            if op == "=" {
                let mut path_indices: Option<Vec<usize>> = if IS_PATH_REGEX.is_match(rhs) {
                    Some(rhs.match_indices('.').map(|(idx, _)| idx).collect())
                } else {
                    None
                };

                if let Some(bindings) = bindings {
                    if let Some(binding) = bindings.get(rhs) {
                        *rhs = binding.clone();
                    } else if let Some(ref indices) = path_indices {
                        // found binding for the longest sub path for which a binding exists
                        let mut found_binding = None;

                        for path_index in indices {
                            let binding = bindings.get(&rhs[0..*path_index]);
                            if let Some(binding) = binding {
                                found_binding = Some((path_index, binding));
                            }
                        }

                        if let Some(found_binding) = found_binding {
                            rhs.replace_range(0..*found_binding.0, found_binding.1);
                            // recalculate path
                            path_indices = if IS_PATH_REGEX.is_match(rhs) {
                                Some(rhs.match_indices('.').map(|(idx, _)| idx).collect())
                            } else {
                                None
                            };
                        }
                    }
                }

                if let Some(path_indices) = path_indices {
                    paths.push(Path::Existing {
                        lhs,
                        path_string: rhs,
                        path_indices,
                        statement_index,
                    });
                }
            }
        }
        Expression::UnaryExpression {
            expression,
            is_declaration,
        } => {
            let ident = expression.trim_matches(|c| char_is_operator(c));
            filter_declaration(is_declaration, ident, variable_scope_stack);
        }
        _ => {}
    }
}

/// Filter multiple assignments to the same variable, making sure only the first one is treated as declaration.
#[inline]
fn filter_declaration(
    is_declaration: &mut bool,
    ident: &str,
    variable_scope_stack: &mut Vec<HashSet<String>>,
) {
    if *is_declaration {
        for variable_scope in variable_scope_stack.iter().rev() {
            if variable_scope.contains(ident) {
                // variable with the same name has already been assigned, skip initialisation
                *is_declaration = false;
                break;
            }
        }

        if *is_declaration {
            variable_scope_stack
                .last_mut()
                .unwrap()
                .insert(String::from(ident));
        }
    }
}

pub enum Statement {
    ExpressionStatement(Expression),
    ExpressionList(Vec<Expression>),
    BlockStatement(BlockStatement),
}

impl Statement {
    /// Appends the groovy string representation of this Statement to the provided buffer, including
    /// the leading indent.
    fn append_groovy_string(&self, buffer: &mut String, indent: &mut usize) {
        match self {
            Statement::ExpressionStatement(expression) => {
                buffer.push_str(&" ".repeat(*indent * INDENT));
                expression.append_groovy_string(buffer);
                buffer.push_str(LINE_SEPARATOR);
            }
            Statement::ExpressionList(expressions) => {
                for expression in expressions {
                    buffer.push_str(&" ".repeat(*indent * INDENT));
                    expression.append_groovy_string(buffer);
                    buffer.push_str(LINE_SEPARATOR);
                }
            }
            Statement::BlockStatement(block) => {
                block.append_groovy_string(buffer, indent);
            }
        }
    }
}

pub enum Expression {
    BinaryExpression {
        lhs: String,
        rhs: String,
        op: String,
        // specifies whether the expression declares a new variable, thus needing the `def` keyword in groovy
        // initially this is true for any encountered assigment using the '=' operator until they are filtered
        // and optimised in the next step
        is_declaration: bool,
    },
    UnaryExpression {
        expression: String,
        is_declaration: bool,
    },
    // Expression that was literally copied from the freemarker template without parsing it further
    // this applies to the entire string within a ${} in freemarker, e.g. ${(foo && bar)} simply becomes one
    // LiteralExpression containing "(foo && bar)", if also applies to expressions supplied to if statements,
    // e.g. [#if foo || bar && baz] becomes an IfStatement with the LiteralExpression "foo || bar && baz".
    LiteralExpression(String),
}

impl Expression {
    /// Appends the groovy string representation of this Expression to the provided buffer.
    fn append_groovy_string(&self, buffer: &mut String) {
        match self {
            Expression::BinaryExpression {
                lhs,
                rhs,
                op,
                is_declaration,
            } => {
                if *is_declaration {
                    buffer.push_str("def ");
                }

                buffer.push_str(lhs);
                buffer.push(' ');
                buffer.push_str(op);
                buffer.push(' ');
                buffer.push_str(rhs);
            }
            Expression::UnaryExpression {
                expression,
                is_declaration,
            } => {
                if *is_declaration {
                    buffer.push_str("def ");
                    buffer.push_str(expression);
                } else {
                    buffer.push_str(expression);
                }
            }
            Expression::LiteralExpression(literal) => {
                buffer.push_str(literal);
            }
        }
    }
}

/// Statement wrapper that, when used in a Vec, allows to perform operations such as inserting and
/// removing statements without changing the size of the Vec, thus not invalidating statement indices.
///
/// When inserting a statement it is placed into the StatementCell located at the provided index by
/// changing the StatementCell to the Multiple variant, setting the original statement as original.
/// If the variant is already a Multiple, the statement is simply inserted at the head of the `inserted`
/// statement Vec. When collecting the StatementCells back to a Vec of Statements, the inserted statements
/// are appended first in the order they appear in the `inserted` Vec with the original statement immediately
/// after.
///
/// When removing a statement, the Single statement is simply set to `None`, if this is the Multiple variant,
/// the original statement is set to `None`, inserted statements cannot be removed again.
pub enum StatementCell {
    Single(Option<Statement>),
    Multiple {
        original: Option<Statement>,
        inserted: Vec<Statement>,
    },
}

impl StatementCell {
    /// Insert a new statement at the index of this StatementCell, if this StatementCell is currently
    /// of type Single, it is changed to a StatementCell of type Multiple with the current Single
    /// statement set as original statement. Else the statement is simply inserted at the head of the
    /// `inserted` Vec.
    pub fn insert_statement(&mut self, statement: Statement) {
        match self {
            StatementCell::Single(optional_statement) => {
                if let Some(curr_statement) = optional_statement.take() {
                    let inserted = vec![statement];
                    *self = StatementCell::Multiple {
                        original: Some(curr_statement),
                        inserted,
                    };
                }
            }
            StatementCell::Multiple { inserted, .. } => {
                inserted.insert(0, statement);
            }
        }
    }

    /// Removes the statement at this index by setting the Single statement to `None` and returning
    /// the previous statement. If this StatementCell is the Multiple variant, the `original` statement
    /// is taken instead, inserted statements cannot be removed again.
    pub fn remove(&mut self) -> Option<Statement> {
        match self {
            StatementCell::Single(optional_statement) => optional_statement.take(),
            StatementCell::Multiple { original, .. } => original.take(),
        }
    }
}
